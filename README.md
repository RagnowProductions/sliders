# <img src="/readmeassets/logo.png">
Sliders is a game developed by RagnowProductions for GitHub Game Off 2023. It involves a shape on a slider scale that changes size and height on the scale according to mathematical functions involving the mouse movement on the Y scale. The objective of the game is to get the slider to output a constant that is equal or in radius of a specific point. 

## Current Leaderboard Stats
Easy Mode: NaN seconds<br>
Medium Mode: NaN seconds<br>
Hard Mode: NaN seconds<br>
High Score (All): 0<br>
Most Conuim: 0 Conium
